const Telegraf     = require('telegraf')
const Telegram     = require('telegraf/telegram')
const Extra        = require('telegraf/extra')
const StateMachine = require('javascript-state-machine');

const bot      = new Telegraf(process.env.BOT_TOKEN)
const telegram = new Telegram(process.env.BOT_TOKEN)
const machines = {}

const FSM = new StateMachine.factory({
    init: 'init',
    transitions: [
        { name: 'ask', from: 'init',   to: 'asked'    },
        { name: 'yes', from: 'asked',  to: 'kicked'   },
        { name: 'yes', from: 'kicked', to: 'banned'   },
        { name: 'no',  from: 'asked',  to: 'accepted' },
        { name: 'no',  from: 'kicked', to: 'accepted' },
    ],
    data: function(ctx) { return {ctx: ctx} },
    methods: {
        onAsk: function() {
            telegram.restrictChatMember(this.ctx.chat.id, this.ctx.message.new_chat_member.id)
            this.ctx.reply(
                `Привет, <a href="tg://user?id=${this.ctx.message.new_chat_member.id}">${this.ctx.message.new_chat_member.first_name}</a>!\n`+
                `Для того, чтобы получить возможность писать в чате, ответь на вопрос:\n\n`+
                `<b>Веришь в Аллаха?</b>\n\n`+
                `У тебя есть 30 секунд.`,

                Extra.HTML().markup((m) =>
                    m.inlineKeyboard([
                        m.callbackButton('Да',  'yes'),
                        m.callbackButton('Нет', 'no'),
                    ])))
            .then(
                result => { this.result = result }
            )
        },
        onEnterBanned: function() {
            telegram.kickChatMember(this.ctx.chat.id, this.ctx.message.new_chat_member.id)
            if (this.result !== undefined) {
                telegram.editMessageText(
                    this.result.chat.id,
                    this.result.message_id,
                    null,
                    `Пользователь ${this.ctx.message.new_chat_member.first_name} дважды не прошел проверку на адекватность`)
            }
        },
        onEnterKicked: function() {
            telegram.kickChatMember(this.ctx.chat.id, this.ctx.message.new_chat_member.id)
            if (this.result !== undefined) {
                telegram.editMessageText(
                    this.result.chat.id,
                    this.result.message_id,
                    null,
                    `Пользователь ${this.ctx.message.new_chat_member.first_name} не прошел проверку на адекватность`)
            }
            telegram.unbanChatMember(this.ctx.chat.id, this.ctx.message.new_chat_member.id)
        },
        onEnterAccepted: function() {
            telegram.promoteChatMember(this.ctx.chat.id, this.ctx.message.new_chat_member.id)
            if (this.result !== undefined) {
                telegram.editMessageText(
                    this.result.chat.id,
                    this.result.message_id,
                    null,
                    `${this.ctx.message.new_chat_member.first_name}, добро пожаловать!`)
            }
        },
    }
});

const getKey = (chat_id, user_id) => `${chat_id}/${user_id}`

bot.on('new_chat_members', async (ctx) => {
    // Ignore update older than 30s
    unix = Math.floor(Date.now() / 1000)
    if (unix - ctx.message.date > 30) { return }

    // Ignore bots
    if (ctx.message.new_chat_member.is_bot) { return }

    // Save id
    id = ctx.message.new_chat_member.id

    // Ask
    key = getKey(ctx.chat.id, ctx.message.new_chat_member.id)
    console.log(`processing new_chat_members for key ${key}`)
    machines[key] = new FSM(ctx)
    machines[key].ask()

    setTimeout(() => {
        try {
            machines[key].yes({editMessageText: () => {}})
        } catch (e) {
            console.error(e);
        }
    }, 30 * 1000)
})

bot.action('yes', async (ctx) => {
    cc = ctx.update.callback_query
    key = getKey(cc.message.chat.id, cc.from.id)
    // key = '-1001365052490/183035577' // debug
    try {
        await ctx.answerCbQuery()
    } catch {
        return
    }

    try {
        await machines[key].yes(ctx);
    } catch (e) {
        console.error(e);
    }
})

bot.action('no', async (ctx) => {
    cc = ctx.update.callback_query
    key = getKey(cc.message.chat.id, cc.from.id)
    // key = '-1001365052490/183035577' // debug
    try {
        await ctx.answerCbQuery()
    } catch{
        return
    }

    try {
        await machines[key].no(ctx);
    } catch (e) {
        console.error(e);
    }
})

bot.launch()
